package final2;

import final1.AbstractTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.config.EncoderConfig.encoderConfig;

public class MyPostTest extends AbsTest {


    @Test
    void getPostTest() {
        given()
                .config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("application/form-data", ContentType.TEXT)))
                .when()
                .formParam("username", "dardi")
                .formParam("password", "2752bd89a8")
                .post().prettyPeek()
                .then()
                .statusCode(200);
    }
}
