package final1;

import org.junit.jupiter.api.BeforeAll;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.sun.tools.javac.main.Option.X;

public abstract class AbstractTest {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String token;
    private static String baseUrl;

    @BeforeAll
    static void initTest() throws IOException {
        configFile = new FileInputStream("src/main/resource/my.properties");
        prop.load(configFile);

        token = prop.getProperty("token");
        baseUrl = prop.getProperty("base_url");
    }

    public static String getToken() {
        return token;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }
}
