package final1;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class MyTest extends AbstractTest {

    @Test
    void getRequestTest() {

        given()
                .queryParam("token", getToken())
                .queryParam("sort", "createdAt")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200);

    }

    @Test
    void getRequestTest2() {

        given()
                .queryParam("token", getToken())
                .queryParam("owner", "notMe")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200);

    }


}


